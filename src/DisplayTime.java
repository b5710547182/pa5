import java.util.Calendar;


/**
 * Show the time for the Clock and also update it.
 * @author Thanawit Gerdprasert
 *
 */
public class DisplayTime implements State{


	/**
	 * Initialize and create other file for this State.
	 */
	public DisplayTime(){

	}
	
	@Override
	public void setButtonPressed(Clock clock) {
		clock.setState(new SetHour());
		Clock alarm = ClockUI.getInstance().getAlarm();
		clock.setHour(alarm.getHour());
		clock.setMinute(alarm.getMinute());
		clock.setSecond(alarm.getSecond());
		clock.setClock(alarm);
	}

	@Override
	public void plusButtonPressed(Clock clock) {
		clock.setState(new DisplayAlarm());
		Clock alarm = ClockUI.getInstance().getAlarm();
		clock.setHour(alarm.getHour());
		clock.setMinute(alarm.getMinute());
		clock.setSecond(alarm.getSecond());
		clock.setClock(alarm);
	}

	@Override
	public void minusButtonPressed(Clock clock) {
		clock.setState(new DisplayAlarm());
		Clock alarm = ClockUI.getInstance().getAlarm();
		clock.setHour(alarm.getHour());
		clock.setMinute(alarm.getMinute());
		clock.setSecond(alarm.getSecond());
		clock.setClock(alarm);
	}

	@Override
	public void updateTime() {
		ClockUI clockUI = ClockUI.getInstance();
		Clock clock = clockUI.getClock();
		clock.getTime().setTime( System.currentTimeMillis() );
		Calendar now = Calendar.getInstance();
		now.setTime(clock.getTime());
		clock.setHour(now.get(Calendar.HOUR_OF_DAY));
		clock.setMinute(now.get(Calendar.MINUTE));
		clock.setSecond(now.get(Calendar.SECOND));
		
		
		if(clock.equalTime(clockUI.getAlarm()))
		{
			clock.setState(new AlarmingState());
			
		}
		
	}

}
