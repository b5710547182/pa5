import java.net.URL;

import javax.swing.ImageIcon;

/**
 * Set Second of Alarm and create it when exit this State.
 * @author Thanawit Gerdprasert.
 *
 */
public class SetSecond implements State {

	private int second;
	private ImageIcon icon;
	private int counter=0;
	
	/**
	 * Initialize and create other file for this State.
	 */
	public SetSecond(){
		second =0;
		ClassLoader loader = this.getClass().getClassLoader();
		URL url = loader.getResource("images/black.gif");
		icon = new ImageIcon(url);
	}
	
	@Override
	public void setButtonPressed(Clock clock) {
		ClockUI clockUI = ClockUI.getInstance();
		clockUI.setAlarm(new Clock(clock.getHour(), clock.getMinute() , clock.getSecond()));
		clock.setState(new DisplayTime());
		clockUI.getAlarmCond().setText("!!! ALARM IS ON !!!");
	}



	@Override
	public void plusButtonPressed(Clock clock) {
		second = clock.getSecond();
		if(second==59)
		{
			clock.setSecond(0);
		}
		else
			clock.setSecond(second+1);
		
	}



	@Override
	public void minusButtonPressed(Clock clock) {
		second = clock.getSecond();
		if(second==0)
		{
			clock.setSecond(59);
		}
		else
			clock.setSecond(second-1);	
	}
	
	@Override
	public void updateTime() {
		counter+=1;
		if(counter%2==1)
		{
			ClockUI.getInstance().getSecondFirstText().setIcon(icon);
			ClockUI.getInstance().getSecondSecondText().setIcon(icon);
			counter =-1;
		}
	}

}
