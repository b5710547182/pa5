import java.net.URL;

import javax.swing.ImageIcon;

/**
 * Set minute of the alarm before creating it.
 * @author Thanawit Gerdprasert.
 *
 */
public class SetMinute implements State {

	int counter;
	private int minute;
	private ImageIcon icon;
	
	/**
	 * Initialize and create other file for this State.
	 */
	public SetMinute(){
		counter =0;
		minute =0;
		ClassLoader loader = this.getClass().getClassLoader();
		URL url = loader.getResource("images/black.gif");
		icon = new ImageIcon(url);
	}
	
	
	
	@Override
	public void setButtonPressed(Clock clock) {
		
		clock.setState(new SetSecond());
	}



	@Override
	public void plusButtonPressed(Clock clock) {
		minute = clock.getMinute();
		if(minute==59)
		{
			clock.setMinute(0);
		}
		else
			clock.setMinute(minute+1);
		
	}



	@Override
	public void minusButtonPressed(Clock clock) {
		minute = clock.getMinute();
		if(minute==0)
		{
			clock.setMinute(59);
		}
		else
			clock.setMinute(minute-1);
		
		
	}



	@Override
	public void updateTime() {
		counter+=1;
		if(counter%2==1)
		{
			ClockUI.getInstance().getMinuteFirstText().setIcon(icon);
			ClockUI.getInstance().getMinuteSecondText().setIcon(icon);
			counter =-1;
		}
		
	}

}
