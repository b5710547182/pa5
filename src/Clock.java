import java.util.Calendar;
import java.util.Date;

/**
 * Clock of Digital Clock that display time.
 * @author Thanawit Gerdprasert.
 *
 */
public class Clock {

	private int hour;
	private int minute;
	private int second;
	private State state;
	private Date time;
	private Clock clock;
	
	/**
	 * Create clock with the received parameter.
	 * @param hour that will be set to
	 * @param minute that will be set to
	 * @param second that will be set to
	 */
	public Clock(int hour , int minute , int second)
	{
		this.setHour(hour);
		this.setMinute(minute);
		this.setSecond(second);
		setState(new DisplayTime());
	}
	
	/**
	 * Create clock with current time.
	 */
	public Clock()
	{
		this.time = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(getTime());
		setHour(now.get(Calendar.HOUR_OF_DAY));
		setMinute(now.get(Calendar.MINUTE));
		setSecond(now.get(Calendar.SECOND));
		setState(new DisplayTime());
	}
	/**
	 * set State of the clock
	 * @param state will be set to
	 */
	public void setState (State state)
	{
		this.state = state;
	}
	/**
	 * use State to perform Set button
	 */
	public void performSetButton(){
		this.state.setButtonPressed(ClockUI.getInstance().getClock());
	}
	/**
	 * use State to perform Plus button
	 */
	public void performPlusButton(){
		this.state.plusButtonPressed(ClockUI.getInstance().getClock());
	}
	/**
	 * use State to perform Minus button
	 */
	public void performMinusButton(){
		this.state.minusButtonPressed(ClockUI.getInstance().getClock());
	}
	
	/**
	 * use State to updateTime.
	 */
	public void updateTime()
	{
		this.state.updateTime();
	}
	
	/**
	 * return hour of clock.
	 * @return hour of clock.
	 */
	public int getHour() {
		return hour;
	}

	/**
	 * set hour of clock
	 * @param hour will be set to
	 */
	public void setHour(int hour) {
		this.hour = hour;
	}
	
	/**
	 * return minute of the clock.
	 * @return minute will be return.
	 */
	public int getMinute() {
		return minute;
	}

	/**
	 * set minute to be as parameter.
	 * @param minute
	 */
	public void setMinute(int minute) {
		this.minute = minute;
	}

	/**
	 * return second of this clock.
	 * @return second of this clock.
	 */
	public int getSecond() {
		return second;
	}

	/**
	 * set second of the clock.
	 * @param second of this clock.
	 */
	public void setSecond(int second) {
		this.second = second;
	}

	/**
	 * return time of the clock.
	 * @return time of the clock.
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * Set the clock to be same as parameter.
	 * @param clock to be as parameter.
	 */
	public void setClock(Clock clock)
	{
		this.clock = clock;
	}
	
	/**
	 * equal method to check the alarm and clock.
	 * @param alarm will be compared to.
	 * @return true if the equals else return false.
	 */
	public boolean equalTime( Clock alarm ){
		Clock clock = ClockUI.getInstance().getClock();
		return(alarm.hour == clock.hour&& alarm.minute == clock.minute && alarm.second == clock.second);
			
	}
}
