/**
 * Application class for the Digital Clock.
 * @author Thanwit Gerdprasert.
 *
 */
public class DigitalClockApp {

/**
 * Use this method to run Digital Clock.
 * @param args not received in this case.
 */
	public static void main(String[] args) {
		ClockUI temp = ClockUI.getInstance();
		temp.run();
	}

}
