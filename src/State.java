/**
 * State of clock in different time.
 * @author Thanawit Gerdprasert.
 *
 */
public interface State {

	/**
	 * Perform action when Set button is pressed.
	 * @param clock that will applied to.
	 */
	public void setButtonPressed(Clock clock);
	
	/**
	 * Perform action when Plus button is pressed.
	 * @param clock that will applied to.
	 */
	public void plusButtonPressed(Clock clock);
	
	/**
	 * Perform action when Minus button is pressed.
	 * @param clock that will applied to.
	 */
	public void minusButtonPressed(Clock clock);
	
	/**
	 * Update time and apply to GUI.
	 */
	public void updateTime();
}
