
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;


/**
 * Alarm alert State for alerting.
 * @author Thanawit Gerdprasert.
 *
 */
public class AlarmingState implements State{

	Clip audioClip;
	private Clip audioSystem;
	private int counter;
	ImageIcon icon;
	private AudioInputStream audioWithInputFile;
	private URL file;
	
	/**
	 * Initialize and create other file for this State.
	 */
	public AlarmingState()
	{
		ClockUI.getInstance().getAlarmCond().setText("!!! ALARM IS RINGING !!!");
		ClassLoader loader = this.getClass().getClassLoader();
		URL url = loader.getResource("images/black.gif");
		icon = new ImageIcon(url);
		
		
        try {
                file = this.getClass().getClassLoader().getResource( "alarm.wav" );
                audioSystem = AudioSystem.getClip();
                audioWithInputFile = AudioSystem.getAudioInputStream( file );
                audioSystem.open( audioWithInputFile );
        } catch (Exception e) {
                e.printStackTrace();
        }
        audioSystem.start();
        audioSystem.loop(Clip.LOOP_CONTINUOUSLY);
	}



	@Override
	public void setButtonPressed(Clock clock) {
		audioSystem.stop();
		ClockUI.getInstance().getAlarmCond().setText("!!! ALARM IS OFF !!!");
		clock.setState(new DisplayTime());

	}

	@Override
	public void plusButtonPressed(Clock clock) {

	}

	@Override
	public void minusButtonPressed(Clock clock) {
	}

	@Override
	public void updateTime() {
		counter+=1;
		ClockUI clockUI = ClockUI.getInstance();
		if(counter%2==1)
		{
			clockUI.getHourFirstText().setIcon(icon);
			clockUI.getHourSecondText().setIcon(icon);
			clockUI.getMinuteFirstText().setIcon(icon);
			clockUI.getMinuteSecondText().setIcon(icon);
			clockUI.getSecondFirstText().setIcon(icon);
			clockUI.getSecondSecondText().setIcon(icon);
			counter =-1;
		}
	}

}
