
import java.net.URL;

import javax.swing.ImageIcon;
/**
 * Set hour of alarm before creating it.
 * @author Thanwit Gerdprasert
 *
 */
public class SetHour implements State{

	private int hour;
	private int counter; 
	ImageIcon icon;
	
	/**
	 * Initialize and create other file for this State.
	 */
	public SetHour(){
		counter=0;
		ClassLoader loader = this.getClass().getClassLoader();
		URL url = loader.getResource("images/black.gif");
		icon = new ImageIcon(url);
	}
	
	@Override
	public void setButtonPressed(Clock clock) {

		clock.setState(new SetMinute());
	}

	@Override
	public void plusButtonPressed(Clock clock) {
		hour = clock.getHour();
		if(hour==23)
		{
			clock.setHour(0);
		}
		else
			clock.setHour(hour+1);
	}

	@Override
	public void minusButtonPressed(Clock clock) {
		hour = clock.getHour();
		if(hour==0)
		{
			clock.setHour(23);
		}
		else
			clock.setHour(hour-1);
	}

	@Override
	public void updateTime() {
		counter+=1;
		if(counter%2==1)
		{
			ClockUI.getInstance().getHourFirstText().setIcon(icon);
			ClockUI.getInstance().getHourSecondText().setIcon(icon);
			counter =-1;
		}
		
	}
	

}
