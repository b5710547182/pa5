import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Singleton GUI for Digital Clock that can show time and set alarm.
 * @author Thanawit Gerdprasert.
 *
 */
public class ClockUI extends JFrame  {

	public Clock alarm;
	public Clock clock;
	private JLabel hourFirstText;
	private JLabel hourSecondText;
	private JLabel minuteFirstText;
	private JLabel minuteSecondText;
	private JLabel secondFirstText;
	private JLabel secondSecondText;
	private JLabel colon1;
	private JLabel colon2;
	private JButton setButton;
	private JButton plusButton;
	private JButton minusButton;
	private static ClockUI clockUI;
	private javax.swing.Timer timer;
	ArrayList<ImageIcon> icons;
	
	
	private int hourFirstDigit;
	private int hourSecondDigit;
	private int minuteFirstDigit;
	private int minuteSecondDigit;
	private int secondFirstDigit;
	private int secondSecondDigit;
	private JLabel alarmCond;
	
	/**
	 * initialize clock and alarm and component.
	 */
	private ClockUI()
	{
		alarm = new Clock(0,0,0);
		clock = new Clock();
		initComponent();
		
	}
	/**
	 * run the GUI.
	 */
	public void run()
	{
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		timer.start();
	}
	
	/**
	 * initialize all GUI component.
	 */
	public void initComponent()
	{
		ClassLoader loader = this.getClass().getClassLoader();
		
		URL zeroURL = loader.getResource("images/0.gif");
		URL oneURL = loader.getResource("images/1.gif");
		URL twoURL = loader.getResource("images/2.gif");
		URL threeURL = loader.getResource("images/3.gif");
		URL fourURL = loader.getResource("images/4.gif");
		URL fiveURL = loader.getResource("images/5.gif");
		URL sixURL = loader.getResource("images/6.gif");
		URL sevenURL = loader.getResource("images/7.gif");
		URL eightURL = loader.getResource("images/8.gif");
		URL nineURL = loader.getResource("images/9.gif");
		
		
		ImageIcon zero = new ImageIcon(zeroURL);
		ImageIcon one = new ImageIcon(oneURL);
		ImageIcon two = new ImageIcon(twoURL);
		ImageIcon three = new ImageIcon(threeURL);
		ImageIcon four = new ImageIcon(fourURL);
		ImageIcon five = new ImageIcon(fiveURL);
		ImageIcon six = new ImageIcon(sixURL);
		ImageIcon seven = new ImageIcon(sevenURL);
		ImageIcon eight = new ImageIcon(eightURL);
		ImageIcon nine = new ImageIcon(nineURL);
		icons = new ArrayList<ImageIcon>();
		icons.add(zero);
		icons.add(one);
		icons.add(two);
		icons.add(three);
		icons.add(four);
		icons.add(five);
		icons.add(six);
		icons.add(seven);
		icons.add(eight);
		icons.add(nine);
		
		super.setTitle("Digital Clock");
		JPanel box = new JPanel();
		box.setLayout(new BoxLayout(box , BoxLayout.Y_AXIS));

		Container upper = new Container();

		upper.setLayout(new FlowLayout());
		
		setHourFirstText(new JLabel(""));
		setMinuteFirstText((new JLabel("")));
		setSecondFirstText((new JLabel("")));
		
		
		setHourSecondText(new JLabel(""));
		setMinuteSecondText(new JLabel(""));
		setSecondSecondText(new JLabel(""));
		
		colon1 = new JLabel("");
		colon2 = new JLabel("");
		

		Container lower = new Container();

		lower.setLayout(new FlowLayout());

		setSetButton((new JButton("Set")));
		setPlusButton((new JButton("+")));
		setMinusButton((new JButton("-")));


		
		upper.add(getHourFirstText());
		upper.add(getHourSecondText());
		upper.add(colon1);
		upper.add(getMinuteFirstText());
		upper.add(getMinuteSecondText());
		upper.add(colon2);
		upper.add(getSecondFirstText());
		upper.add(getSecondSecondText());

		lower.add( getSetButton() );
		lower.add( getPlusButton() );
		lower.add( getMinusButton() );

		Container alarmContainer = new Container();
		alarmContainer.setLayout(new FlowLayout(FlowLayout.CENTER));
		setAlarmCond(new JLabel("!!! ALARM IS OFF !!!"));
		alarmContainer.add(getAlarmCond());
		box.add(alarmContainer);
		box.add(upper);
		box.add(lower);

		super.add(box);

		super.pack();

		getSetButton().addActionListener(new setListener());
		getPlusButton().addActionListener(new plusListener());
		getMinusButton().addActionListener(new minusListener());

		
		
		
		
		int delay = 500; // milliseconds
		ActionListener task = new ActionListener() {
		 public void actionPerformed(ActionEvent evt) {
			
			printTime();
			clock.updateTime();
		 }
		};
		timer = new javax.swing.Timer(delay, task);

	}

	/**
	 * print time for the GUI.
	 */
	public void printTime()
	{
		Clock clock = ClockUI.getInstance().getClock();
		
		hourFirstDigit = clock.getHour() / 10;
		hourSecondDigit = clock.getHour() % 10;
		minuteFirstDigit = clock.getMinute()/10;
		minuteSecondDigit = clock.getMinute() %10;
		secondFirstDigit = clock.getSecond() /10;
		secondSecondDigit = clock.getSecond() %10;
		
		hourFirstText.setIcon(icons.get(hourFirstDigit));		
		hourSecondText.setIcon(icons.get(hourSecondDigit));
		minuteFirstText.setIcon(icons.get(minuteFirstDigit));
		minuteSecondText.setIcon(icons.get(minuteSecondDigit));
		secondFirstText.setIcon(icons.get(secondFirstDigit));
		secondSecondText.setIcon(icons.get(secondSecondDigit));
		
		ClassLoader loader = this.getClass().getClassLoader();
		URL colon = loader.getResource("images/colon.gif");
		colon1.setIcon(new ImageIcon(colon));
		colon2.setIcon(new ImageIcon(colon));
		
		
		ClockUI.getInstance().pack();
	}
	
	/**
	 * create ClockUI if it is null else return clockUI.
	 * @return Singleton of ClockUI.
	 */
	public static ClockUI getInstance()
	{
		if(clockUI == null)
		{
			clockUI = new ClockUI();
		}
		
		return clockUI;
	}
	
	
	/**
	 * return left-side of hour.
	 * @return left side of hour.
	 */
	public JLabel getHourFirstText()
	{
		return this.hourFirstText;
	}
	
	/**
	 * return left-side of minute.
	 * @return left side of minute.
	 */
	public JLabel getMinuteFirstText() {
		return minuteFirstText;
	}

	
	/**
	 * set minute of the first side.
	 * @param minuteText will be set to.
	 */
	public void setMinuteFirstText(JLabel minuteText) {
		this.minuteFirstText = minuteText;
	}

	/**
	 * get the first second text.
	 * @return first second text.
	 */
	public JLabel getSecondFirstText() {
		return secondFirstText;
	}

	/**
	 * Set the second text.
	 * @param secondText
	 */
	public void setSecondFirstText(JLabel secondText) {
		this.secondFirstText = secondText;
	}

	/**
	 * get the set button.
	 * @return
	 */
	public JButton getSetButton() {
		return setButton;
	}

	/**
	 * set the set button
	 * @param setButton
	 */
	public void setSetButton(JButton setButton) {
		this.setButton = setButton;
	}

	/**
	 * return the minus button 
	 * @return button returned.
	 */
	public JButton getMinusButton() {
		return minusButton;
	}

	/**
	 * set the minus button
	 * @param minusButton
	 */
	public void setMinusButton(JButton minusButton) {
		this.minusButton = minusButton;
	}

	/**
	 * get the plus button
	 * @return the plus button
	 */
	public JButton getPlusButton() {
		return plusButton;
	}

	/**
	 * set the plus button 
	 * @param plusButton will be set to
	 */
	public void setPlusButton(JButton plusButton) {
		this.plusButton = plusButton;
	}
	/**
	 * get the clock 
	 * @return the clock
	 */
	public Clock getClock()
	{
		return this.clock;
	}

	/**
	 * set the hour of first teXt
	 * @param hourText will be set.
	 */
	public void setHourFirstText(JLabel hourText) {
		this.hourFirstText = hourText;
	}

	/**
	 * get hour of second text.
	 * @return
	 */
	public JLabel getHourSecondText() {
		return hourSecondText;
	}
	
	/**
	 * set hour of the second text
	 * @param hourSecondText
	 */
	public void setHourSecondText(JLabel hourSecondText) {
		this.hourSecondText = hourSecondText;
	}
	
	/**
	 * get minute of second text
	 * @return
	 */
	public JLabel getMinuteSecondText() {
		return minuteSecondText;
	}

	/**
	 * set the minute text
	 * @param minuteSecondText
	 */
	public void setMinuteSecondText(JLabel minuteSecondText) {
		this.minuteSecondText = minuteSecondText;
	}

	/**
	 * get the second text
	 * @return second text
	 */
	public JLabel getSecondSecondText() {
		return secondSecondText;
	}
 
	/**
	 * set the text
	 * @param secondSecondText
	 */
	public void setSecondSecondText(JLabel secondSecondText) {
		this.secondSecondText = secondSecondText;
	}
	/**
	 * set the alarm to be as parameter.
	 * @param clock that alarm will be set to.
	 */
	public void setAlarm(Clock clock)
	{
		this.alarm = clock;
	}
	/**
	 * return alarm.
	 * @return alarm.
	 */
	public Clock getAlarm()
	{
		return this.alarm; 
	}
	/**
	 * get the alarm text condition
	 * @return
	 */
	public JLabel getAlarmCond() {
		return alarmCond;
	}
	/**
	 * set alarm text condition
	 * @param alarmCond
	 */
	public void setAlarmCond(JLabel alarmCond) {
		this.alarmCond = alarmCond;
	}
	
}

/**
 * Action Listener for Set Button
 * @author Thanawit Gerdprasert.
 *
 */
class setListener implements ActionListener {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ClockUI.getInstance().getClock().performSetButton();
	}
}
/**
 * Action Listener for plus button
 * @author Thanawit Gerdprasert
 *
 */
class plusListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		ClockUI.getInstance().getClock().performPlusButton();
	}
}
/**
 * Action Listener for minus button.
 * @author Thanawit Gerpdrasert
 *
 */
class minusListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		ClockUI.getInstance().getClock().performMinusButton();
	}
}